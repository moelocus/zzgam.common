﻿namespace ZZgam.UnityBaseTools
{
    public class PersistenceDataJson<T> : PersistenceData where T : PersistenceDataJson<T>
    {
        protected T dataObject;
        public PersistenceDataJson(string file) : base(file) { }

        public override bool Read()
        {
            return JsonFile.ReadFile(path,out dataObject);
        }

        public override bool Write()
        {
            return JsonFile.WriteFile(path, dataObject);
        }
    }
}
