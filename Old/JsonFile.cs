﻿using System;
using System.IO;
using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public class JsonFile
    {
        public static bool ReadFile<T>(string file, out T data)
        {
            if (!string.IsNullOrEmpty(file))
            {
                try
                {
                    if (File.Exists(file))
                    {
                        data = JsonUtility.FromJson<T>(File.ReadAllText(file));
                        if (data != null)
                        {
                            return true;
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("Exception during read json file:{0}", e.Message);
                }
            }
            data = default(T);
            return false;
        }

        public static bool WriteFile(string file, object data)
        {
            if (!string.IsNullOrEmpty(file))
            {
                try
                {
                    if (!Directory.Exists(Path.GetDirectoryName(file)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(file));
                    }
                    File.WriteAllText(file, JsonUtility.ToJson(data));
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("Exception during write json file:{0}", e.Message);
                }
            }
            else
            {
                Debug.LogError("file path name is emputy!");
            }
            return false;
        }
    }
}
