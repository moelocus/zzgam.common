﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Game_Play.Utils.Unity_Tester
{
    public abstract class FixedUpdateTester : MonoBehaviour
    {
        private void FixedUpdate()
        {
            OnFixedUpdate();
        }

        protected abstract void OnFixedUpdate();
    }
}
