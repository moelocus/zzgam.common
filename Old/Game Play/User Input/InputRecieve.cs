﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;

namespace ZZgam.UnityBaseTools.Game_Play.User_Input
{
    class InputRecieve : SingletonGlobalMonobehaviour<InputRecieve>
    {
        static bool enableUpdateAxes;
        public static bool UpdateAxesEnable
        {
            get
            {
                return enableUpdateAxes;
            }
            set
            {
                if (Instance != null)
                {
                    enableUpdateAxes = value;
                }
            }
        }

        static bool enableUpdateKeys;
        public static bool UpdateKeysEnable
        {
            get
            {
                return enableUpdateKeys;
            }
            set
            {
                if (Instance != null)
                {
                    enableUpdateKeys = value;
                }
            }
        }

        private void Update()
        {
            if (enableUpdateAxes)
            {
                BindingAxes.Instance.UpdateAxes.Invoke();
            }
            if (enableUpdateKeys)
            {
                BindingKeys.Instance.UpdateKeys.Invoke();
            }
        }
    }
}
