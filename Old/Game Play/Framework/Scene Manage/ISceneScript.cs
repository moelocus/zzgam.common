﻿using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;

namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    /// <summary>
    /// 场景脚本接口
    /// </summary>
    public interface ISceneScript
    {
        /// <summary>
        /// 场景名称
        /// </summary>
        string SceneName { get; }

        /// <summary>
        /// 加载场景方法
        /// </summary>
        /// <param name="resultAction">加载完成时回调,返回是否成功加载</param>
        void LoadScene(ZZAction<bool> resultAction = null);
    }
}
