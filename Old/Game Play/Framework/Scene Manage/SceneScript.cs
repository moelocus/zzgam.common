﻿using UnityEngine.SceneManagement;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;

namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    public abstract class SceneScript : ISceneScript
    {
        public string SceneName { get; protected set; }

        public SceneScript(string sceneName)
        {
            SceneName = sceneName;
            SetAllListeners(true);
        }

        ~SceneScript()
        {
            SetAllListeners(false);
        }

        private void SetAllListeners(bool add)
        {
            SceneControl.Instance.SceneLoaded.AutoListener(add, SceneLoaded);
            SceneControl.Instance.SceneUnloaded.AutoListener(add, SceneUnloaded);
        }

        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (scene.name.Equals(SceneName))
            {
                OnSceneLoaded();
            }
        }

        private void SceneUnloaded(Scene scene)
        {
            if (scene.name.Equals(SceneName))
            {
                OnSceneUnloaded();
            }
        }

        public void LoadScene(ZZAction<bool> resultAction = null)
        {
            SceneControl.Instance.LoadScene.Invoke(SceneName, resultAction);
        }

        protected abstract void OnSceneLoaded();

        protected abstract void OnSceneUnloaded();
    }
}
