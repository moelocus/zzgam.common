﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;

namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    /// <summary>
    /// 场景操作控制者
    /// </summary>
    class SceneControl : SingletonGlobalMonobehaviour<SceneControl>
    {
        internal ZZAction<string, ZZAction<bool>> LoadScene = new ZZAction<string, ZZAction<bool>>();
        internal ZZAction<string, ZZAction<bool>> UnloadScene = new ZZAction<string, ZZAction<bool>>();

        internal ZZAction<Scene, LoadSceneMode> SceneLoaded = new ZZAction<Scene, LoadSceneMode>();
        internal ZZAction<Scene> SceneUnloaded = new ZZAction<Scene>();
        internal ZZAction<Scene, Scene> ActiveSceneChanged = new ZZAction<Scene, Scene>();

        protected override void SetupListeners(bool add)
        {
            base.SetupListeners(add);

            LoadScene.AutoListener(add, DoLoadScene);
            UnloadScene.AutoListener(add, DoUnloadScene);

            if (add)
            {
                SceneManager.sceneLoaded += SceneLoaded.Invoke;
                SceneManager.sceneUnloaded += SceneUnloaded.Invoke;
                SceneManager.activeSceneChanged += ActiveSceneChanged.Invoke;
            }
            else
            {
                SceneManager.sceneLoaded -= SceneLoaded.Invoke;
                SceneManager.sceneUnloaded -= SceneUnloaded.Invoke;
                SceneManager.activeSceneChanged -= ActiveSceneChanged.Invoke;
            }
        }

        
        private void DoLoadScene(string sceneName, ZZAction<bool> onComplete)
        {
            //StartCoroutine(CoroutineLoadScene(sceneName, onComplete));

            //TODO:临时修改为加载单场景，之后做拓展
            if (SceneManager.GetSceneByName(sceneName).isLoaded)
            {
                Debug.LogErrorFormat("场景{0}已经存在，不再加载", sceneName);
                return;
            }
            SceneManager.LoadScene(sceneName);
            if (onComplete != null)
            {
                onComplete.Invoke(SceneManager.GetSceneByName(sceneName).isLoaded);
            }
        }

        private void DoUnloadScene(string scene, ZZAction<bool> onComplete)
        {
            StartCoroutine(CoroutineUnloadScene(scene, onComplete));
        }

        private IEnumerator CoroutineLoadScene(string sceneName, ZZAction<bool> onComplete)
        {
            if (SceneManager.GetSceneByName(sceneName).isLoaded)
            {
                Debug.LogErrorFormat("场景{0}已经存在，不再加载", sceneName);
                yield break;
            }
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            if (onComplete != null)
            {
                onComplete.Invoke(SceneManager.GetSceneByName(sceneName).isLoaded);
            }
        }

        private IEnumerator CoroutineUnloadScene(string sceneName, ZZAction<bool> onComplete)
        {
            if (!SceneManager.GetSceneByName(sceneName).isLoaded)
            {
                Debug.LogErrorFormat("场景{0}没有加载，无法卸载", sceneName);
                yield break;
            }
            yield return SceneManager.UnloadSceneAsync(sceneName);
            if (onComplete != null)
            {
                onComplete.Invoke(!SceneManager.GetSceneByName(sceneName).isLoaded);
            }
        }
    }
}
