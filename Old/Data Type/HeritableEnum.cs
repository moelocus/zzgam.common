﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ZZgam.UnityBaseTools.Data_Type
{
    /// <summary>
    /// 可继承枚举类型
    /// </summary>
    public class HeritableEnum<T> : IComparable<HeritableEnum<T>>, IEquatable<HeritableEnum<T>> where T : HeritableEnum<T>
    {
        static int counter = 0;
        static Hashtable hashtable = new Hashtable();
        static List<HeritableEnum<T>> list = new List<HeritableEnum<T>>();
        string name;
        int value;

        protected HeritableEnum(string name)
        {
            this.name = name;
            value = ++counter;
            list.Add(this);
            if (!hashtable.ContainsKey(value))
            {
                hashtable.Add(value, this);
            }
        }

        protected HeritableEnum(string name, int value) : this(name)
        {
            this.value = value;
            counter = value;
        }

        public override string ToString()
        {
            return name.ToString();
        }

        public static explicit operator HeritableEnum<T>(int i)
        {
            if (hashtable.ContainsKey(i))
            {
                return list[i];
            }
            return new HeritableEnum<T>(i.ToString(), i);
        }

        public static explicit operator int(HeritableEnum<T> e)
        {
            return e.value;
        }
        
        public static void ForEach(Action<HeritableEnum<T>> action)
        {
            foreach (HeritableEnum<T> item in list)
            {
                action(item);
            }
        }

        public virtual int CompareTo(HeritableEnum<T> other)
        {
            return value.CompareTo(other.value);
        }

        public virtual bool Equals(HeritableEnum<T> other)
        {
            return value.Equals(other.value);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is HeritableEnum<T>))
                return false;
            return value == ((HeritableEnum<T>)obj).value;
        }

        public override int GetHashCode()
        {
            HeritableEnum<T> std = (HeritableEnum<T>)hashtable[value];
            if (std.name == name)
                return base.GetHashCode();
            return std.GetHashCode();
        }

        public static bool operator !=(HeritableEnum<T> e1, HeritableEnum<T> e2)
        {
            return e1.value != e2.value;
        }

        public static bool operator <(HeritableEnum<T> e1, HeritableEnum<T> e2)
        {
            return e1.value < e2.value;
        }

        public static bool operator <=(HeritableEnum<T> e1, HeritableEnum<T> e2)
        {
            return e1.value <= e2.value;
        }

        public static bool operator ==(HeritableEnum<T> e1, HeritableEnum<T> e2)
        {
            return e1.value == e2.value;
        }

        public static bool operator >(HeritableEnum<T> e1, HeritableEnum<T> e2)
        {
            return e1.value > e2.value;
        }

        public static bool operator >=(HeritableEnum<T> e1, HeritableEnum<T> e2)
        {
            return e1.value >= e2.value;
        }
    }
}
