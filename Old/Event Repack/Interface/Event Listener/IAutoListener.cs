﻿namespace ZZgam.UnityBaseTools.Event_Repack.Interface.Event_Listener
{
    interface IAutoListener<T>
    {
        void AutoListener(bool add, T listener);
    }
}
