﻿namespace ZZgam.UnityBaseTools.Event_Repack.Interface.Event_Listener
{
    interface IRemoveListener<T>
    {
        void RemoveListener(T listener);
    }
}
