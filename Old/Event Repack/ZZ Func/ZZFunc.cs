﻿using System;
using UnityEngine;
using ZZgam.UnityBaseTools.Event_Repack.Interface.Event_Listener;

namespace ZZgam.UnityBaseTools.Event_Repack.ZZ_Func
{
    interface IZZFunc<TFunc, TResult> : IAddListener<TFunc>, IRemoveListener<TFunc>, IAutoListener<TFunc>
    {
        void RemoveAllListeners();
    }

    public class ZZFunc<TResult> : IZZFunc<Func<TResult>, TResult>
    {
        event Func<TResult> ThisEvent;

        public void AddListener(Func<TResult> func)
        {
            ThisEvent += func;
        }

        public void RemoveListener(Func<TResult> func)
        {
            ThisEvent -= func;
        }

        public void AutoListener(bool add, Func<TResult> func)
        {
            if (add)
            {
                AddListener(func);
            }
            else
            {
                RemoveListener(func);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public TResult Invoke()
        {
            if (ThisEvent != null)
            {
                return ThisEvent.Invoke();
            }
            return default(TResult);
        }
    }
    public class ZZFunc<T, TResult> : IZZFunc<Func<T, TResult>, TResult>
    {
        event Func<T, TResult> ThisEvent;

        public void AddListener(Func<T, TResult> func)
        {
            ThisEvent += func;
        }

        public void RemoveListener(Func<T, TResult> func)
        {
            ThisEvent -= func;
        }

        public void AutoListener(bool add, Func<T, TResult> func)
        {
            if (add)
            {
                AddListener(func);
            }
            else
            {
                RemoveListener(func);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public TResult Invoke(T t)
        {
            if (ThisEvent != null)
            {
                return ThisEvent.Invoke(t);
            }
            return default(TResult);
        }
    }
    public class ZZFunc<T1, T2, TResult> : IZZFunc<Func<T1, T2, TResult>, TResult>
    {
        event Func<T1, T2, TResult> ThisEvent;

        public void AddListener(Func<T1, T2, TResult> func)
        {
            ThisEvent += func;
        }

        public void RemoveListener(Func<T1, T2, TResult> func)
        {
            ThisEvent -= func;
        }

        public void AutoListener(bool add, Func<T1, T2, TResult> func)
        {
            if (add)
            {
                AddListener(func);
            }
            else
            {
                RemoveListener(func);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public TResult Invoke(T1 t1, T2 t2)
        {
            if (ThisEvent != null)
            {
                return ThisEvent.Invoke(t1, t2);
            }
            return default(TResult);
        }

        internal void Invoke(KeyCode keyCode, object onKeys)
        {
            throw new NotImplementedException();
        }
    }
    public class ZZFunc<T1, T2, T3, TResult> : IZZFunc<Func<T1, T2, T3, TResult>, TResult>
    {
        event Func<T1, T2, T3, TResult> ThisEvent;

        public void AddListener(Func<T1, T2, T3, TResult> func)
        {
            ThisEvent += func;
        }

        public void RemoveListener(Func<T1, T2, T3, TResult> func)
        {
            ThisEvent -= func;
        }

        public void AutoListener(bool add, Func<T1, T2, T3, TResult> func)
        {
            if (add)
            {
                AddListener(func);
            }
            else
            {
                RemoveListener(func);
            }
        }

        public void RemoveAllListeners()
        {
            ThisEvent = null;
        }

        public TResult Invoke(T1 t1, T2 t2, T3 t3)
        {
            if (ThisEvent != null)
            {
                return ThisEvent.Invoke(t1, t2, t3);
            }
            return default(TResult);
        }
    }
}
