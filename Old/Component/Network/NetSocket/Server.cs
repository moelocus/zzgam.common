﻿using Packages.zzgam.common.Component.Network.NetSocket;
using System.Net;

namespace Assets.ZZgam
{
    public class Server : SocketPeel
    {
        public DataEventBytes OnDataRecv = new DataEventBytes();

        public Server(IPAddress iPAddress, int port, int backlog = 1) : base()
        {
            Listen(iPAddress, port, backlog);
        }

        public Server(int port, int backlog = 1) : this(IPAddress.Any, port, backlog) { }

        protected override void OnRecieved(byte[] data)
        {
            if (OnDataRecv != null)
            {
                OnDataRecv.Invoke(data);
            }
        }
    }
}
