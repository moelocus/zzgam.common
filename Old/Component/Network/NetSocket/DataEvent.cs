﻿using UnityEngine.Events;

namespace Assets.ZZgam
{
    public class DataEventBytes : UnityEvent<byte[]> { }
    public class DataEventString : UnityEvent<string> { }
}