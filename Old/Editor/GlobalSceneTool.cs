﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UE = UnityEditor.Editor;

namespace Assets.ZZ.Scenes.Editor
{
    class GlobalSceneTool : UE
    {
        public SceneAsset sceneLoadLevel = null;
        public SceneAsset sceneLoadAsset = null;
        public SceneAsset sceneLoadScript = null;

        //[MenuItem("ZZ/OpenGlobalScenes")]
        static void OpenGlobalScenes()
        {
            var c = CreateInstance<GlobalSceneTool>();
            if (c.sceneLoadLevel)
            {
                EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(c.sceneLoadLevel));
            }
            if (c.sceneLoadAsset)
            {
                EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(c.sceneLoadAsset), OpenSceneMode.Additive);
            }
            if (c.sceneLoadScript)
            {
                EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(c.sceneLoadScript), OpenSceneMode.Additive);
            }
        }
    }
}
