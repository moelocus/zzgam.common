﻿using UnityEditor;
using UE = UnityEditor.Editor;

namespace Assets.ZZ.Scenes.Editor
{
    class XRConfig:UE
    {
        /// <summary>
        /// 开启XR
        /// </summary>
        //[MenuItem("ZZ/Utils/XREnable")]
        static void XREnable()
        {
            PlayerSettings.virtualRealitySupported = true;
        }

        /// <summary>
        /// 关闭XR
        /// </summary>
        //[MenuItem("ZZ/Utils/XRDisable")]
        static void XRDisable()
        {
            PlayerSettings.virtualRealitySupported = false;
        }
    }
}
