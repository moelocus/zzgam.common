﻿using ZZgam.UnityBaseTools.Data_Type;

namespace ZZgam.UnityBaseTools.Design_Manage.MonoBehaviour_Script
{
    public class ScriptID : HeritableEnum<ScriptID>
    {
        protected ScriptID(string name) : base(name) { }
        protected ScriptID(string name, int value) : base(name, value) { }

        public static ScriptID NullScriptID = new ScriptID("NullScriptID", 0);
    }
}
