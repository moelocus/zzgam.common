﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Utils
{
    public class Res
    {
        /// <summary>
        /// 加载资源中的物体并添加到子物体
        /// </summary>
        /// <param name="path"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static bool LoadResToChild(string path, Transform parent)
        {
            var go = Resources.Load(path) as GameObject;
            if (go)
            {
                GameObject goo = Object.Instantiate(go);
                goo.transform.SetParent(parent);
                goo.name = go.name;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 加载资源中的物体并添加到子物体
        /// 获取这个物体
        /// </summary>
        /// <param name="path"></param>
        /// <param name="parent"></param>
        /// <param name="resObject"></param>
        /// <returns></returns>
        public static bool LoadResToChildGetObject(string path, Transform parent, out GameObject resObject)
        {
            var go = Resources.Load(path) as GameObject;
            if (go)
            {
                resObject = Object.Instantiate(go);
                resObject.transform.SetParent(parent);
                resObject.name = go.name;
                return true;
            }
            resObject = null;
            return false;
        }

        /// <summary>
        /// 加载资源中的物体并添加到子物体
        /// 获取这个物体的某个脚本
        /// </summary>
        /// <typeparam name="T">脚本类型</typeparam>
        /// <param name="path"></param>
        /// <param name="parent"></param>
        /// <param name="targetScript"></param>
        /// <returns></returns>
        public static bool LoadResToChildGetScript<T>(string path, Transform parent, out T targetScript)
        {
            GameObject go = Resources.Load(path) as GameObject;
            if (go)
            {
                var goo = Object.Instantiate(go);
                goo.transform.SetParent(parent);
                goo.name = go.name;

                targetScript = goo.GetComponent<T>();
                if (targetScript != null)
                {
                    return true;
                }
            }
            targetScript = default(T);
            return false;
        }
    }
}
