﻿using System;
using UnityEngine;

namespace ZZgam.UnityBaseTools.Utils
{
    public static class And
    {
        public static bool AndTrue(params Func<bool>[] trueActionList)
        {
            foreach (var action in trueActionList)
            {
                if (!action.Invoke())
                {
                    Debug.LogErrorFormat("运算{0}为假，停止后续运算", action.Method.Name);
                    return false;
                }
            }
            return true;
        } 
    }
}
