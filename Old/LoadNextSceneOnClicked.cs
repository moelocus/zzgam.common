﻿using UnityEngine;
using UnityEngine.UI;

namespace ZZgam.UnityBaseTools
{
    [RequireComponent(typeof(Button))]
    class LoadNextSceneOnClicked : MonoBehaviour
    {
        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(() => SceneManagerExtern.LoadNextScene());
        }
    }
}
