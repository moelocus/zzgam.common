﻿using System.IO;
using UnityEngine;

public class LoadFiles : MonoBehaviour
{
    public string refPath;
    public string extName;
    public UnityEventString eventLoadFile = new UnityEventString();

    string[] allFiles;
    void Start()
    {
        string path = Path.Combine(Application.streamingAssetsPath, refPath);
        if (Directory.Exists(path))
        {
            allFiles = Directory.GetFiles(path);
            foreach (var item in allFiles)
            {
                var cuts = item.Split('.');
                string exn = cuts[cuts.Length - 1];
                if (exn.Equals(extName))
                {
                    eventLoadFile.Invoke(item);
                }
            }
        }
    }
}
