﻿using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public static class ExternAnimation
    {
        public static void Pause(this Animation target)
        {
            if (target.clip)
            {
                target[target.clip.name].speed = 0;
            }
        }

        public static void Resume(this Animation target)
        {
            if (target.clip)
            {
                target[target.clip.name].speed = 1;
            }
        }
    }
}
