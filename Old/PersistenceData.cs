﻿namespace ZZgam.UnityBaseTools
{
    public abstract class PersistenceData
    {
        protected string path;
        public PersistenceData(string path)
        {
            this.path = path;
        }
        public abstract bool Read();
        public abstract bool Write();
    }
}
