﻿using ZZgam.UnityBaseTools.Data_Type;

namespace ZZgam.UnityBaseTools.Design_Pattern.State_Pattern
{
    public class StateID : HeritableEnum<StateID>
    {
        protected StateID(string name) : base(name) { }
        protected StateID(string name, int value) : base(name, value) { }

        public static StateID NullStateID = new StateID("NullStateID", 0);
    }
}
