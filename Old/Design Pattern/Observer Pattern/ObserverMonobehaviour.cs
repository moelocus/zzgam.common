﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Design_Pattern.Observer_Pattern
{
    public abstract class ObserverMonobehaviour : MonoBehaviour
    {
        protected virtual void Start()
        {
            SetAllListeners(true);
        }

        protected virtual void OnDestroy()
        {
            SetAllListeners(false);
        }

        protected abstract void SetAllListeners(bool add);
    }
}
