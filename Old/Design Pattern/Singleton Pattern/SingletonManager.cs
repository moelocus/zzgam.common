﻿namespace ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern
{
    public class SingletonManager<TSingletonManager, TSingletonBehaviour> : Singleton<TSingletonManager>
        where TSingletonManager : SingletonManager<TSingletonManager, TSingletonBehaviour>, new()
        where TSingletonBehaviour : SingletonBehaviour<TSingletonBehaviour, TSingletonManager>
    {
        public SingletonBehaviour<TSingletonBehaviour, TSingletonManager> Behaviour { get; private set; }

        public void BindBehaviour(SingletonBehaviour<TSingletonBehaviour, TSingletonManager> behaviour)
        {
            Behaviour = behaviour;
        }
    }
}
