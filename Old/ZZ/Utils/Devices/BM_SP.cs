﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;

namespace Assets.ZZ.Utils.Devices
{
    public class BM_SP : SingletonManager<BM_SP, MB_SP>
    {
        public ZZAction<SerialPortConfig> EOpenPort = new ZZAction<SerialPortConfig>();

        public ZZAction EClosePort = new ZZAction();

        public ZZAction<int> EStartRead = new ZZAction<int>();

        public ZZAction EStopRead = new ZZAction();

        public ZZAction<byte[]> EDataRead = new ZZAction<byte[]>();
    }
}
