﻿using System;
using UnityEngine;

namespace Assets.ZZ.Scenes.Loaders.Script
{
    public abstract class CSharpScriptSelector : MonoBehaviour, ICSharpScriptSelector
    {
        public abstract Type GetScript(string scriptID);
    }
}
