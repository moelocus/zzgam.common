﻿using System;

namespace Assets.ZZ.Scenes.Loaders.Script
{
    public interface ICSharpScriptSelector
    {
        Type GetScript(string scriptID);
    }
}
