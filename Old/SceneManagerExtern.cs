﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace ZZgam.UnityBaseTools
{
    public class SceneManagerExtern : SceneManager
    {
        static Stack<int> stack = new Stack<int>();

        /// <summary>
        /// 切换到下一个场景
        /// </summary>
        public static void LoadNextScene(bool pushStack = true)
        {
            int buildIndex = GetActiveScene().buildIndex;
            if (buildIndex++ >= 0)
            {
                LoadScene(buildIndex);
                if (pushStack)
                {
                    stack.Push(buildIndex);
                }
            }
        }

        /// <summary>
        /// 返回上一个场景
        /// </summary>
        public static void BackLastScene()
        {
            if (stack.Count > 0)
            {
                LoadScene(stack.Pop());
            }
        }
    }
}
