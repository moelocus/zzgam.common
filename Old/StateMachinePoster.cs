﻿using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public class StateMachinePoster : StateMachineBehaviour
    {
        public bool enableStateExit = false;
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateExit(animator, stateInfo, layerIndex);
            if (enableStateExit)
            {
                IOnStateExitReceiver receiver = animator.GetComponent<IOnStateExitReceiver>();
                if (receiver != null)
                {
                    receiver.OnStateExit(stateInfo, layerIndex);
                }
                else
                {
                    Debug.LogError("there are no component IOnStateExitReceiver found on Animator");
                }
            }
        }
    }
}
