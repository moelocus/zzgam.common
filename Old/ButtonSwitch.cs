﻿using UnityEngine;
using UnityEngine.Events;

namespace ZZgam.UnityBaseTools
{
    /// <summary>
    /// 按钮开关
    /// 每次按动会在两值间切换
    /// </summary>
    public class ButtonSwitch : ExternButton
    {
        public bool SwitchValue { get; private set; }

        SwitchButtonClickedEvent clickedEvent;

        public new SwitchButtonClickedEvent onClick
        {
            get
            {
                if (clickedEvent == null)
                {
                    clickedEvent = new SwitchButtonClickedEvent();
                }
                return clickedEvent;
            }
            set
            {
                clickedEvent = value;
            }
        }

        public class SwitchButtonClickedEvent : UnityEvent<bool> { }

        protected override void Awake()
        {
            base.Awake();
            base.onClick.AddListener(OnBaseClicked);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            base.onClick.RemoveListener(OnBaseClicked);
        }

        private void OnBaseClicked()
        {
            onClick.Invoke(SwitchValue);
        }

        SelectionState lastState;
        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            if (lastState == SelectionState.Pressed && state == SelectionState.Highlighted)
            {
                SwitchValue = !SwitchValue;
                base.DoStateTransition(SwitchValue ? SelectionState.Pressed : SelectionState.Normal, instant);
            }
            lastState = state;
        }
    }
}
